<?php if(isset($_SESSION["username"])){
	header("Location: /");
} ?>
<?php include "../assets/includes/top.inc.php"; ?>
							
							<div class="pageWidth">
				    			<div class="widget">
				    				<div class="subHeading">Registrierung</div>
				    				<div style="margin: 10px; font-size: 13px;">
				    					<p>Die Registrierung erfolgt in folgenden, einfachen Schritten:</p>
				    					<ul>
				    						<li><p><b>Schritt #1:</b> Betrete den Minecraft-Server <span style="color: #FF0000">pvp-hub.net</span></p></li>
				    						<li><p><b>Schritt #2:</b> Gib in den Chat ein: <span style="color: #FF0000">/register &lt;Passwort&gt; &lt;Passwort&gt;</span> (Dabei ist "&lt;Passwort&gt;" mit deinem zuk&uuml;nftigem Passwort zu ersetzen)</p></li>
				    						<li><p><b>Schritt #3:</b> Jetzt kannst du dich auf unserer Website einloggen!</p></li>
				    					</ul>
				    				</div>
				    			</div>
							 </div>
			    			
<?php include "../assets/includes/bottom.inc.php"; ?>