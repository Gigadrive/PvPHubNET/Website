<?php include "../assets/includes/top.inc.php"; ?>
				<div class="pageWidth">
					<div class="widget">
				    	<div class="subHeading">Impressum</div>
				    	<div style="margin: 10px; font-size: 13px;">
				    		<p>Angaben gem&auml;&szlig; &sect; 5 TMG:<br/><br/></p>
							<p>Mehdi Baaboura<br />
							Marktplatz 5<br />
							52428 J&uuml;lich<br />
							</p>
						</div>
				    </div>
				    
				    <div class="widget">
				    	<div class="subHeading">Kontakt</div>
				    	<div style="margin: 10px; font-size: 13px;">
				    		<table width="100%" border="0">
				    			<tr>
				    				<td width="50%" valign="top" style="text-align: right">
				    					Allgemeiner Support
				    				</td>
				    				<td width="50%" valign="top">
				    					<a href="mailto:support@pvp-hub.net">support@pvp-hub.net</a>
				    				</td>
				    			</tr>
				    		</table>
				    	</div>
				    </div>
				    
				    <div class="widget">
				    	<div class="subHeading">Quellenangaben</div>
				    	<div style="margin: 10px; font-size: 13px;">
				    		<ul>
				    			<li><b>Social Media Icons</b> von <a href="https://www.iconfinder.com/search/?q=iconset%3Aisometric-social-media-icons" target="_blank">Lokas Software</a></li>
				    			<li><b>PvP-Hub.net Logo</b> von <a href="http://www.zeryther.tk" target="_blank">Zeryther</a></li>
				    			<li><b>Minecraft 2D & 3D Avatare</b> von <a href="https://minotar.net" target="_blank">Minotar.net</a> und <a href="https://cravatar.eu" target="_blank">Cravatar.eu</a></li>
				    		</ul>
						</div>
				    </div>
			    </div>
<?php include "../assets/includes/bottom.inc.php"; ?>