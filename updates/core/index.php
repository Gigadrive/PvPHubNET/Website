<?php require("/var/www/html/assets/includes/top.inc.php"); ?>
<?php

function get_json($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function get_repo($repoName, $user){
    return json_decode(get_json("https://bitbucket.org/api/2.0/repositories/$user/$repoName"), true);
}

function get_commits($repo, $user){
    return json_decode(get_json("https://bitbucket.org/api/2.0/repositories/$user/$repo/commits"), true);
}

$commits = get_commits("pvphubcore", "PvPHubNET");
$repository = get_repo("pvphubcore", "PvPHubNET");

$commit1 = $commits[0];
$commit2 = $commits[1];
$commit3 = $commits[2];

$author1 = $commit1["author"]["user"]["username"];
$author2 = $commit2["author"]["user"]["username"];
$author3 = $commit3["author"]["user"]["username"];

$message1 = $commit1["message"];
$message2 = $commit2["message"];
$message3 = $commit3["message"];



?>
<div class="pageWidth">
    <div class="widget">
        <div class="subHeading">PvPHubCore Changelogs - Gehostet auf Bitbucket.org</div>
        <div style="margin: 10px; font-size: 13px;">
            <table width="100%" border="0">
                <tr>
                    <td width="25%">
                        <b>Commit-ID</b>
                    </td>
                    <td width="25%">
                        <b>Autor</b>
                    </td>
                    <td width="50%">
                        <b>Commit-Nachricht</b>
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                        id
                    </td>
                    <td width="25%">
                        <?php print $author1; ?>
                    </td>
                    <td width="50%">
                        <?php print $message1; ?>
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                        id
                    </td>
                    <td width="25%">
                        <?php print $author2; ?>
                    </td>
                    <td width="50%">
                        <?php print $message2; ?>
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                        id
                    </td>
                    <td width="25%">
                        <?php print $author3; ?>
                    </td>
                    <td width="50%">
                        <?php print $message3; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php require("/var/www/html/assets/includes/bottom.inc.php"); ?>