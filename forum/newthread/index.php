<?php include "../../assets/includes/top.inc.php";?>
<?php if(isset($_GET["cid"])){
    $cid = $_GET["cid"];
} else {
    die();
}

if(!isset($_SESSION["id"])){
    die();
}

?>
			<div class="pageWidth">
			   <div class="inv-widget">
                               <h1>Neuen Thread erstellen</h1>
                               <div class="widget">
				<div class="subHeading">Neuen Thread erstellen</div>
					<div style="margin: 10px; font-size: 13px;">
                                            <table width="100%" border="0">
                                                <form action="/assets/includes/form_newThread.php" method="post">
                                                    <input type="text" class="hugeTextBar" placeholder="Titel" name="title"/>
                                                    <textarea name="content" placeholder="Beschreibung"></textarea><br/>
                                                    <input type="hidden" value="<?php print $cid; ?>" name="cid"/>
                                                    <input class="ticketButton" type="submit" name="submit" value="Thread erstellen" style="float: left" />
                                                </form>
                                            </table>
					</div>
				</div>
			   </div>
			</div>
<?php include "../../assets/includes/bottom.inc.php"; ?>
