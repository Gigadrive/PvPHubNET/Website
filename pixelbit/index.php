<?php require("/home/apache2/www/assets/includes/top.inc.php"); ?>
			<div class="pageWidth">
				<div class="inv-widget">
                    <table width="100%" border="0">
                        <tr>
                            <td width="25%" valign="top">
                                <h1 class="ownerTitle">
                                    Manager
                                </h1>
                                <ul class="o-list">
			    							<li>
			    								<img alt="Zeryther" title="Zeryther" src="https://minotar.net/helm/Zeryther/72.png"/>
			    								<a href="/player/73b417dcd1e645d8af06895eeb5222a5">Zeryther</a>
			    								<p>Manager / Owner</p>
			    							</li>
			    							<li>
			    								<img alt="VipsNiceMinePlay" title="VipsNiceMinePlay" src="https://minotar.net/helm/VipsNiceMinePlay/72.png"/>
			    								<a href="/player/e6e3192bd6d54460a07577e7b20eaa8b">VipsNiceMinePlay</a>
			    								<p>Manager / Owner</p>
			    							</li>
			    							<li>
			    								<img alt="Materia__" title="Materia__" src="https://minotar.net/helm/Materia__/72.png"/>
			    								<a class="adminLink" href="/player/a430b7778905432c91c438de4b28ec2b">Materia__</a>
			    								<p>Manager</p>
			    							</li>
			    						</ul>
                                <h1 class="buildTitle">
                                    Member
                                </h1>
                                    <a href="/player/839910fd351742489fda180c81daff00"><img class="avatar" alt="Killcrafter42" title="Killcrafter42" src="https://minotar.net/helm/Killcrafter42/50.png"/></a>
								    <a href="/player/374850e60c154dc5b82ccb9cfc85db40"><img class="avatar" alt="jick_saw" title="jick_saw" src="https://minotar.net/helm/jick_saw/50.png"/></a>
								    <a href="/player/0736036cf393486aa68de7e7c13aaf2a"><img class="avatar" alt="Kiiiwe" title="Kiiiwe" src="https://minotar.net/helm/Kiiiwe/50.png"/></a>
								    <a href="/player/41fbd8edae444cb8bfc09a06317516f5"><img class="avatar" alt="zocker1904" title="zocker1904" src="https://minotar.net/helm/zocker1904/50.png"/></a>
                            </td>
                            <td width="75%" valign="top" style="padding-left: 15px">
                                <h1 class="sectionTitle">Team PixelBit</h1>
                                <center><i>hier kommt ein bild hin :O</i></center>
                                <p>Team PixelBit ist das offizielle PvP-Hub.net Bau-Team. Hier kommt der gr&ouml;&szlig;te Teil unserer Maps und Bauten her.</p>
                                <h1 class="devTitle">Werde ein Teil des Teams!</h1>
                                <p>Werde noch heute ein Teil des PixelBit Teams und bewerbe dich <a href="/support/ticket/new/">hier</a>!<br/>
                                Beachte, dass dabei folgendes in deiner Bewerbung stehen sollte:</p>
                                <ul>
                                    <li>Dein Ingame Name</li>
                                    <li>Dein Alter</li>
                                    <li>Was du am besten Kanns (Themen, Bau-Stile, ..)</li>
                                    <li>Deine Erfahrungen mit WorldEdit und VoxelSniper</li>
                                    <li>Bilder von voherigen Bauten. Diese sollten fertig sein und dein Talent zeigen.</li>
                                    <li><i>Tipp: Schreibe in vollen S&auml;tzen</i></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
				</div>
			</div>
<?php require("/home/apache2/www/assets/includes/bottom.inc.php"); ?>
