<?php include "../assets/includes/top.inc.php";?>
<?php
$_POST["autoNick"] = "false";
if(!isset($_SESSION["id"])){
    die();
}
?>

<?php
    $sql_u = mysql_query("SELECT * FROM users WHERE id=" . $_SESSION["id"] . "");
    while($row_u = mysql_fetch_array($sql_u)){
        $showStats = $row_u["setting_showStats"];
        $autoNick = $row_u["setting_autoNick"];
        $allowFriendRequests = $row_u["setting_allowFriendRequests"];
        $serverLocation = $row_u["setting_setverLocation"];
        $tsid = $row_u["teamspeak_id"];
        $signature = $row_u["signature"];
    }
?>
			<div class="pageWidth">
                            <div class="inv-widget">
                                <!--<form action="/assets/includes/form_updateSettings.php" method="post">-->
                                    <div class="widget">
                                        <div class="subHeading">Server-Einstellungen</div>
                                        <div style="margin: 10px; font-size: 13px;">
                                            <form action="/assets/includes/form_serverSettings.php" method="post">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Zeige Server?<br/>
                                                                <small>M&ouml;chtest du, dass andere deinen Server auf der Website sehen?</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <select name="showServer">
                                                                <option value="1"<?php if($serverLocation == 1){print' selected';} ?>>Zeige Server</option>
                                                                <option value="0"<?php if($serverLocation == 0){print' selected';} ?>>Verstecke Server</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <?php if($_SESSION["rank"] >= 3){ ?>
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Auto-Nick?<br/>
                                                                <small>M&ouml;chtest du, beim Server join automatisch genickt werden?</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <select name="autoNick">
                                                                <option value="1"<?php if($autoNick == 1){print' selected';} ?>>Automatisch nicken</option>
                                                                <option value="0"<?php if($autoNick == 0){print' selected';} ?>>Echten Namen anzeigen</option>
                                                            </select>
                                                        </td>
                                                    </tr>    
                                                    <?php } ?>
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Zeige Stats?<br/>
                                                                <small>M&ouml;chtest du, dass andere deine Statistiken mit /stats einsehen k&ouml;nnen?</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <select name="showStats">
                                                                <option value="1"<?php if($showStats == 1){print' selected';} ?>>Zeige Stats</option>
                                                                <option value="0"<?php if($showStats == 0){print' selected';} ?>>Verstecke Stats</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Freundschaftsanfragen?<br/>
                                                                <small>M&ouml;chtest du, dass andere dir Freundschaftsanfragen senden k&ouml;nnen?</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <select name="allowFriends">
                                                                <option value="1"<?php if($allowFriendRequests == 1){print' selected';} ?>>Erlaube Anfragen</option>
                                                                <option value="0"<?php if($allowFriendRequests == 0){print' selected';} ?>>Ignoriere Anfragen</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <input type="submit" value="Speichern" name="submit"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="widget">
                                        <div class="subHeading">Website-Einstellungen</div>
                                        <div style="margin: 10px; font-size: 13px;">
                                            <form action="/assets/includes/form_websiteSettings.php" method="post">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Signatur<br/>
                                                                <small>Wird unter deinen Foren-Posts angezeigt.</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <textarea name="signature"><?php print $signature; ?></textarea>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <input type="submit" value="Speichern" name="submit"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="widget">
                                        <div class="subHeading">TeamSpeak-Einstellungen</div>
                                        <div style="margin: 10px; font-size: 13px;">
                                            <form action="/assets/includes/form_tsSettings.php" method="post">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td width="25%" valign="top">
                                                            <p>
                                                                Unique ID (Eindeutige ID)<br/>
                                                                <small>Wird f&uuml;r TeamSpeak Server Gruppen verwendet.</small>
                                                            </p>
                                                        </td>
                                                        <td width="75%" valign="top">
                                                            <input type="text" name="tsid" value="<?php print $tsid; ?>"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <input type="submit" value="Speichern" name="submit"/>
                                            </form>
                                        </div>
                                    </div>
                                <!--</form>-->
			    </div>
			</div>
<?php include "../assets/includes/bottom.inc.php"; ?>
