<?php 
require("../assets/includes/top.inc.php"); 
require_once("../assets/includes/minetoweb.inc.php");

if($mconline) { 
	$mcs = '<font color="#00CC00">ONLINE</font>'; 
	if($mc[2] == "WARTUNG") {
		$mcs .= ' (<font color="#AA0000">WARTUNG</font>)';
	}

	$motd = MineToWeb($mc[6]);
	$players = $mc[9] . "/" . $mc[4];
} 
else {
	$mcs = '<font color="#CC0000">OFFLINE</font>';
	$motd = "/";
	$players = "/";
}
	
if($tsonline) { 
	$tss = '<font color="#00CC00">ONLINE</font>';
	$tsusers = $ts[4] . "/" . $ts[3];
	$channels = $ts[5];
}
else {
	$tss = '<font color="#CC0000">OFFLINE</font>';
	$tsusers = "/";
	$channels = "/";
}

?>
			<div class="pageWidth">
				<table width="100%" border="0">
			    	<tr>
			    		<td width="50%" valign="top">
							<div class="widget">
								<center>
								<?php if($mconline) { ?>
									<img src="<?php print $mc[7]; ?>" alt="PvP-Hub.net Server Icon" />
								<?php } else { ?>
									<img src="/assets/images/minecraft.png" alt="Minecraft Icon" />
								<?php } ?>
								</center>
								<table align="center">
									<tr align="center">
										<td style="text-align: right;">IP:</td>
										<td style="text-align: left;">eu.pvp-hub.net</td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">Status:</td>
										<td style="text-align: left;"><?php print $mcs; ?></td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">Spieler:</td>
										<td style="text-align: left;"><?php print $players; ?></td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">MOTD:</td>
										<td style="text-align: left;"><?php print $motd; ?></td>
									</tr>
								</table>
							</div>
						</td>
						<td width="50%" valign="top">
							<div class="widget">
								<center>
									<img src="/assets/images/teamspeak.png" alt="Teamspeak Icon" />
								</center>
								<table align="center">
									<tr align="center">
										<td style="text-align: right;">IP:</td>
										<td style="text-align: left;">ts.pvp-hub.net</td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">Status:</td>
										<td style="text-align: left;"><?php print $tss; ?></td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">User:</td>
										<td style="text-align: left;"><?php print $tsusers; ?></td>
									</tr>
									<tr align="center">
										<td style="text-align: right;">Channel:</td>
										<td style="text-align: left;"><?php print $channels; ?></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
<?php require("../assets/includes/bottom.inc.php"); ?>