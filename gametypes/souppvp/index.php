<?php include "../../assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
			    <div class="widget">
			    	<div class="subHeading">SoupPvP - Erkl&auml;rung</div>
			    	<div style="margin: 10px; font-size: 13px">
                                    <p>SoupPvP ist ein sich sehr schnell entwickelnder Spielmodus und au&szlig;erdem einer unserer Secondary-Mini-Gametypes. Das ganze basiert auf dem PvP (Player vs. Player) Prinzip. Man spawnt in einer Lobby in der man sein Kit ausw&auml;hlen kann um loszubattlen. Es gibt eine Vielzahl an Kits. Darunter Standart-Klassen, die nur ein Schwert + R&uuml;stung haben, aber auch andere die in der Lage sind Zauberspr&uuml;che zu verwenden. Die Kit-Auswahl ist im Shop (zu finden in der SPVP-Lobby) durch Tausch gegen PvP-Hub Coins erweiterbar.</p>
                                    <center>
                                        <a href="maps/"><button>Maps</button></a>
                                        <a href="toplist/"><button>Topliste</button></a>
                                    </center>
			    	</div>
			    </div>
			 </div>
<?php include "../../assets/includes/bottom.inc.php"; ?>