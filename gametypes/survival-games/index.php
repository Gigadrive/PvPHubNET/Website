<?php include "../../assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
			    <div class="widget">
			    	<div class="subHeading">Survival Games - Erkl&auml;rung</div>
			    	<div style="margin: 10px; font-size: 13px">
                                    <p>Dieser Spielmodus lehnt an den Film "Die Tribute von Panem" an. Man spawnt auf einer selbstgebauten Karte mit ca. 24 Spawnpl&auml;tzen. Auf der ganzen Karte sind zuf&auml;llig Kisten verteilt, die man nun versucht zu pl&uuml;ndern. Mit den gewonnen Sachen kann man seine Gegner bezwingen. Ziel ist es als letzter auf der Karte zu &uuml;berleben.</p>
                                    <p>Wir werden oft gefragt, warum es bei uns Hunger Games hei&szlig;t und nicht Survival Games - HG sei der falsche Begriff f&uuml;r das Spielprinzip auf unseren Maps.<br /><br />
                                    Wir sehen das aber so: In den Spielen des Original Hunger Games-Filmes starten alle K&auml;mpfer in einem Kreis und m&uuml;ssen sich in einer zuvor pr&auml;parierten Arena bek&auml;mpfen. Genauso funktioniert das bei uns auch. Das andere Spielkonzept, das auf einigen US-Servern als "Hunger Games" zu finden ist, stellt f&uuml;r uns eher Survival Games dar: Man startet auf einer einfachen Map, ohne Hilfen, Fallen und Spezialgebiete, und muss dort ums &uuml;berleben k&auml;mpfen.</p>
                                    <center>
                                        <a href="maps/"><button>Maps</button></a>
                                        <a href="toplist/"><button>Topliste</button></a>
                                    </center>
			    	</div>
			    </div>
			 </div>
<?php include "../../assets/includes/bottom.inc.php"; ?>