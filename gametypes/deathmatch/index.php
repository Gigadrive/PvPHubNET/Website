<?php include "../../assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
			    <div class="widget">
			    	<div class="subHeading">Deathmatch - Erkl&auml;rung</div>
			    	<div style="margin: 10px; font-size: 13px">
                                    <p>Zu Beginn des Spiels werden du und deine Mitspieler in 4 Teams eingeteilt. Team Iron, Team Gold, Team Diamond und Team Lapis. Jedes Team hat Team-Leben. Diese werden verbraucht, sobald ein Spieler aus dem jeweiligem Team stirbt. Sobald die Team-Leben eines Teams auf 0 fallen, scheidet es aus. W&auml;hrend des Kampfes k&ouml;nnen &uuml;berall auf der Karte Items gefunden werden. Diese machen sich durch einen Redstone-Block bemerkbar. Macht man einen Rechtsklick auf den Redstone-Block, verschwindet dieser und man bekommt ein zuf&auml;lliges Item. Das Team, das als letztes lebt, gewinnt.</p>
                                    <center>
                                        <a href="maps/"><button>Maps</button></a>
                                        <a href="toplist/"><button>Topliste</button></a>
                                    </center>
			    	</div>
			    </div>
			 </div>
<?php include "../../assets/includes/bottom.inc.php"; ?>