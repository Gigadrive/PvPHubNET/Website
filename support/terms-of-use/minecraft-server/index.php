<?php include "/var/www/html/assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
				<table width="100%" border="0">
					<tr>
						<td width="75%" valign="top">
							<div class="widget">
								<div class="subHeading">Minecraft Server Regeln</div>
								<div style="margin: 10px; font-size: 13px">
									<p>Wer auf unserem Minecraft-Server spielt, ist verpflichtet die hier aufgelisteten Regeln zu beachten. Bei Missachtung kann je nach Vergehen, eine Verwarnung, ein Kick, ein zeitlich begrenzter Ausschluss vom Server oder ein kompletter Ausschluss vom Server erfolgen. Sollte dir ein Regelversto&szlig; auffallen, melde dies bitte umgehend hier im Forum.
									
									<ul>
									<li>Verhalte dich deinen Mitspielern gegen&uuml;ber respektvoll. Das hei&szlig;t Beleidigungen, Mobbing oder gar sexuelle Belästigungen werden NICHT geduldet!</li>
									<li>Rassistische, antisemitische oder gewaltverherrlichende sowie pornografische oder sonstwie nicht jugendfreie Äu&szlig;erungen, Spielernamen und Symbole (z.b. auf Skins) sind ebenfalls verboten!</li>
									<li>Werbung, egal f&uuml;r was geworben wird (au&szlig;er es handelt sich um Sachen wie ein Video vom PvP-Hub Team auf YouTube oder einen PvP-Hub Livestream) wird auf unserem Server in keinster Weise geduldet!</li>
									<li>Jegliche Modifikationen die dem User das Spiel vereinfachen sind auf unserem Server verboten!*</li>
									<li>Teams mit mehr als 2 Spielern sind verboten! Dies gilt f&uuml;r alle PvP Modi. In "One in the Gun" hingegen sind GAR KEINE Teams erlaubt!</li>
									<li>Anweisungen vom PvP-Hub Team sind folge zu leisten!</li>
									<li>Spam im Chat ist verboten!</li>
									<li>Dauerhaftes Aufhalten an einem Ort (oder auch "Camping") ist nicht erlaubt!</li>
									<li>Das Umgehen von Strafen mit einem weiterem Minecraft Account ist verboten!</li>
									<li>Das Weitergeben von InGame Infos die man erst als Spectator bekommt (oder auch "Ghosting") ist verboten!</li>
									<li>Wir behalten uns vor jeden Spieler, der gegen diese Regeln verst&ouml;&szlig;t vom Spiel auszuschlie&szlig;en. Team-Mitglieder, VIPs und Premium-User sind davon <b>NICHT</b> ausgeschlossen!</li>
									</ul><br /><br />
									
									Weiterhin gilt das Sprichwort "Unwissenheit sch&uuml;tzt vor Strafe nicht!". Solltest du meinen zu Unrecht gebannt worden zu sein, kannst du hier im Forum dazu Stellung nehmen!<br /><br />
									
									<b>*Weitere Infos:</b> Am wenigsten Geduldet werden Modifikationen wie der "Nodus" Hacking Client, der "Platinum" Hacking Client, die "NoSlow" Modifikation und die GermanGriefer-Modifikation. Die BetterSprinting Modifikation hingegen ist erlaubt, da es f&uuml;r uns, das Admin-Team einfach unm&ouml;glich ist diese zu kontrollieren.
									<b>EDIT:</b> BetterSprinting ist ab 1.7 in den Vanilla Minecraft Client integriet, also wird jeder sie benutzen. </p>
								</div>
							</div>
						</td>
						<td width="25%" valign="top">
							<?php require_once "/var/www/html/assets/includes/support_right.inc.php"; ?>
						</td>
					</tr>
				</table>
			 </div>
<?php include "/var/www/html/assets/includes/bottom.inc.php"; ?>