<?php include "/var/www/html/assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
				<table width="100%" border="0">
					<tr>
						<td width="75%" valign="top">
							<div class="widget">
								<div class="subHeading">TeamSpeak3 Server Regeln</div>
								<div style="margin: 10px; font-size: 13px">
									<p>Wer auf unseren TeamSpeak3 Server joint, ist verpflichtet die hier aufgelisteten Regeln zu beachten. Bei Missachtung kann je nach Vergehen, eine Verwarnung, ein Kick, ein zeitlich begrenzter Ausschluss vom Server oder ein kompletter Ausschluss vom Server folgen. Sollte dir ein Regelversto&szlig; eines Mitspielers auffallen, melde dies bitte umgehend einem Team-Mitglied.<br /><br />
									<ul>
									<li>Verhalte dich deinen Mitspielern gegen&uuml;ber respektvoll. Das hei&szlig;t Beleidigungen, Mobbing oder gar sexuelle Bel&auml;stigungen werden NICHT geduldet!</li>
									<li>Rassistische antisemitische oder gewaltverherrlichende sowie pornographische oder sonstwie nicht jugendfreie &auml;u&szlig;erungen, Namen und Symbole (z. B. auf Avataren) sind ebenfalls verboten und haben auf dem Server nichts verloren!</li>
									<li>Werbung, egal f&uuml;r was geworben wird (au&szlig;er es handelt sich um Sachen wie ein Video vom PvP-Hub Team auf YouTube oder einen PvP-Hub Livestream) wird auf unserem Server in keinster Weise geduldet!</li>
									<li>Anweisungen vom PvP-Hub Team sind folge zu leisten!</li>
									<li>Spam ist verboten!</li>
									<li>Wir behalten uns vor jeden Spieler, der gegen diese Regeln verst&ouml;&szlig;t vom Spiel auszuschlie&szlig;en. Team-Mitglieder, VIPs und Premium-User sind davon NICHT ausgeschlossen!</li>
									</ul><br /><br />
									
									Weiterhin gilt das Sprichwort "Unwissenheit sch&uuml;tzt vor Strafe nicht!" Solltest du meinen zu Unrecht gebannt worden zu sein, kannst du hier um Forum dazu Stellung nehmen!</p>
								</div>
							</div>
						</td>
						<td width="25%" valign="top">
							<?php require_once "/var/www/html/assets/includes/support_right.inc.php"; ?>
						</td>
					</tr>
				</table>
			 </div>
<?php include "/var/www/html/assets/includes/bottom.inc.php"; ?>