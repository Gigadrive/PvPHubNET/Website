<?php include "/var/www/html/assets/includes/top.inc.php"; ?>
			<div class="pageWidth">
				<table width="100%" border="0">
					<tr>
						<td width="75%" valign="top">
							<div class="widget">
								<div class="subHeading">Store Nutzungsbedingungen</div>
								<div style="margin: 10px; font-size: 13px">
									<p>Diese Nutzungsbedingungen gelten f&uuml;r jeden Kunden des PvP-Hub Stores, erreichbar unter <a href="http://store.pvp-hub.net" target="_blank">http://store.pvp-hub.net</a> oder <a href="http://pvphub.buycraft.net" target="_blank">http://pvphub.buycraft.net</a>, zus&auml;tzlich zu den Minecraft-Server-Regeln, den TeamSpeak-Regeln und den Foren-Regeln (im Folgenden "PvP-Hub.net-Regeln" und "Regeln"). Mit dem Kauf eines Artikels im PvP-Hub Store stimmt jeder Kunde diesen Bedingungen zu.</p>
									<p> </p>
									<p>Alle Zahlungen an PvP-Hub.net sind Zahlungen f&uuml;r diejenigen virtuellen Einheiten (im Folgenden "Premium-Mitgliedschaften" und "Chips"), die im Einkauf enthalten sind. Die Zahlungen sind endg&uuml;ltig und nicht mehr r&uuml;ckg&auml;ngig zu machen; es gibt keine R&uuml;ckerstattung.<br/>
									Im Fall eines Bannes aufgrund von Verst&ouml;&szlig;en gegen die PvP-Hub.net-Regeln, existiert kein Anspruch auf eine Erstattung. Die Regeln von PvP-Hub.net k&ouml;nnen sich jederzeit &auml;ndern und sind immer von jedem Spieler zu beachten und zu befolgen.<br/>
									Regel&auml;nderungen und das Bannen von Spielern liegt allein im Ermessen der Mitglieder des PvP-Hub.net-Teams. Eine Regel&auml;nderung ist auch f&uuml;r diejenigen Spieler g&uuml;ltig, deren Eink&auml;ufe vor dieser Regel&auml;nderung get&auml;tig wurden. PvP-Hub.net ist nicht dazu verpflichtet, die Spieler und insbesondere die K&auml;ufer &uuml;ber Regel&auml;nderungen zu benachrichtigen.<br/>
									Es gibt keine Garantie daf&uuml;r, dass der Server nach einem Kauf betreten werden kann. Ebenfalls wird nicht daf&uuml;r garantiert, mit einem Chip verschiedene Items, Klassen oder andere Funktionen f&uuml;r sich beanspruchen zu k&ouml;nnen. Wenn der Betrieb von PvP-Hub.net eingestellt wird, verfallen jegliche virtuelle Einheiten. Geht durch einen Fehler auf PvP-Hub.net eine Premium-Mitgliedschaft oder ein Chip verloren, gibt es kein Recht auf eine Erstattung. Premium-Mitgliedschaften & Chips sind virtuell und haben keinen Wert.
								</div>
							</div>
						</td>
						<td width="25%" valign="top">
							<?php require_once "/var/www/html/assets/includes/support_right.inc.php"; ?>
						</td>
					</tr>
				</table>
			 </div>
<?php include "/var/www/html/assets/includes/bottom.inc.php"; ?>