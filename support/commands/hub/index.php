<?php require_once "/var/www/html/assets/includes/top.inc.php"; ?>
<div class="pageWidth"><br />
<?php if(isset($_SESSION["username"])){ ?>
	<div style="float: right;">
		<a href="/support/ticket/new"><input class="ticketButton" type="button" value="Support Ticket senden" /></a>
	</div>
<?php }
?>
<table width="100%" border="0">
	<tr>
		<td width="75%" valign="top">
			<div class="widget">
				<div class="subHeading">Erkl&auml;rung</div>
				<div style="margin: 10px; font-size: 13px;">
					<p>Mit dem <b>/hub</b> Befehl kommst du immer wieder zur Lobby zur&uuml;ck.</p>
				</div>
			</div>
			
			<div class="widget">
				<div class="subHeading">Richtige Verwendung</div>
				<div style="margin: 10px; font-size: 13px;">
					<p>/hub</p>
				</div>
			</div>
			
			<div class="widget">
				<div class="subHeading">Aliase</div>
				<div style="margin: 10px; font-size: 13px;">
					<p>Folgende Befehle haben den selben Nutzen:</p>
					<ul>
						<li>/lobby</li>
						<li>/leave</li>
						<li>/l</li>
						<li>/q</li>
						<li>/quit</li>
					</ul>
				</div>
			</div>
		</td>
		<td width="25%" valign="top">
			<?php require_once "/var/www/html/assets/includes/support_right.inc.php"; ?>
		</td>
	</tr>
</table>
</div>
<br />
<?php require_once "/var/www/html/assets/includes/bottom.inc.php"; ?>