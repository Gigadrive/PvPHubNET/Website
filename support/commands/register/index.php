<?php require_once "/var/www/html/assets/includes/top.inc.php"; ?>
<div class="pageWidth"><br />
<?php if(isset($_SESSION["username"])){ ?>
	<div style="float: right;">
		<a href="/support/ticket/new"><input class="ticketButton" type="button" value="Support Ticket senden" /></a>
	</div>
<?php }
?>
<table width="100%" border="0">
	<tr>
		<td width="75%" valign="top">
			<div class="widget">
				<div class="subHeading">Erkl&auml;rung</div>
				<div style="margin: 10px; font-size: 13px;">
					<p>Mit dem <b>/register</b> Befehl kann ein Account auf der PvP-Hub Website erstellt werden. Eine genaue Erkl&auml;rung gibt es <a href="/register">hier</a>.</p>
				</div>
			</div>
			
			<div class="widget">
				<div class="subHeading">Richtige Verwendung</div>
				<div style="margin: 10px; font-size: 13px;">
					<p>/register &lt;Passwort&gt; &lt;Passwort wdh.&gt;</p>
				</div>
			</div>
		</td>
		<td width="25%" valign="top">
			<?php require_once "/var/www/html/assets/includes/support_right.inc.php"; ?>
		</td>
	</tr>
</table>
</div>
<br />
<?php require_once "/var/www/html/assets/includes/bottom.inc.php"; ?>