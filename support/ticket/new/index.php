<?php session_start();

if(isset($_SESSION["username"])) {
	if(!isset($_POST["submit"]) || !isset($_POST["description"])|| !isset($_POST["category"])){
		// User has not submitted ticket
		require("/home/apache2/www/assets/includes/top.inc.php");
		?>
		
		<div class="inv-widget">
			<div class="pageWidth">
				<table width="100%" border="0">
					<tr>
						<td width="50%" valign="top">
							<div class="widget">
								<div class="subHeading">Neues Ticket erstellen</div>
								<div style="margin: 10px; font-size: 13px;">
									<form action="index.php" method="post">
										<table width="100%" border="0">
											<tr>
												<td width="25%"><p class="infoDesc">Kategorie <a href="#" title="Gib die Kategorie an die am meisten auf dein Problem zutrifft. Beispiele: Hast du einen Bug auf unserer Website gefunden? -> Bug (Website), Wurdest du gebannt? -> Bann">(?)</a></p></td>
												<td width="75%">
													<select name="category">
														<option value="account">Account-Verwaltung</option>
														<option value="bug_ts3">Bug (TeamSpeak3)</option>
														<option value="bug_mc">Bug (Minecraft)</option>
														<option value="bug_web">Bug (Website)</option>
														<option value="premium">Premium- / Chip-K&auml;ufe</option>
														<option value="ban">Bann (TS3/Forum/MC)</option>
                                                        <option value="apply">Team-Bewerbung</option>
													</select>
												</td>
											</tr>
											<tr><td><hr></td><td><hr></td></tr>
											<tr>
												<td width="25%"><p class="infoDesc">Beschreibung <a href="#" title='Beschreibe hier dein Problem so genau wie m&ouml;glich, damit unsere Moderatoren dir helfen k&ouml;nnen. Beispiel: Ich wurde am 01.01.1970 um 23:59 von [Moderator] XY mit dem Grund "Hacking" gebannt, hacke jedoch nicht. Beweis: http://youtube.com/watch?v=B.E.I.S.P.I.E.L'>(?)</a></p></td>
												<td width="75%">
													<textarea name="description"></textarea>
												</td>
											</tr>
											<tr><td><hr></td><td><hr></td></tr>
											<tr>
												<td width="25%"><p class="infoDesc">&nbsp;</p></td>
												<td width="75%">
													<input type="submit" value="Abschicken" name="submit"/>
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</td>
						<td width="25%" valign="top">
							<?php require_once "/home/apache2/www/assets/includes/support_right.inc.php"; ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<?php
		require("/home/apache2/www/assets/includes/bottom.inc.php");
	} else {
		// User has submitted ticket
		$sender = $_SESSION["username"];
		$category = $_POST["category"];
		$content = $_POST["description"];
		$submit = $_POST["submit"];
		$is_solved = "n";
		
		require("/home/apache2/www/assets/includes/top.inc.php");
		$sql = mysql_query("INSERT INTO `users`.`tickets` (`id`, `reporter`, `description`, `date`, `answered_by`, `answer`, `answer_date`, `category`, `is_solved`) VALUES (NULL, '" . $sender . "', '" . $content . "', CURRENT_TIMESTAMP, NULL, NULL, NULL, '" . $category . "', 'n');");
		if($sql){ ?>
			
			
			<div class="pageWidth">
				<div class="alert alert-success">
					<p><i class="fa fa-check"></i> Dein Ticket wurde abgesendet.</p>
				</div>
			</div>
			
			<?php require("/home/apache2/www/assets/includes/bottom.inc.php");
		?>
			
		<?php } else { ?>
			
			<div class="pageWidth">
				<div class="alert alert-failure">
					<p><i class="fa fa-check"></i> <?php print mysql_error(); ?></p>
				</div>
			</div>
			
			<?php require("/home/apache2/www/assets/includes/bottom.inc.php");
			
		}
	}
} else {
	// User not logged in
	$_SESSION['lerr'] = 1;
	$_SESSION['lerrmsg'] = "Du musst dich zuerst anmelden um ein Ticket zu erstellen";
	header("Location: https://www.pvp-hub.net/login/?redirect=ticket");
}
?>