<?php
	session_start();
	if(isset($_SERVER['REQUEST_URI'])) {
		$ru = $_SERVER['REQUEST_URI'];
		if($ru == "/assets/error/404manager.php") {
			header("Location: https://new.pvp-hub.net");
			return;
		}

		if(substr($ru, 0, 8) == "/player/") {
			$arr = explode("/", $ru);
			if(isset($arr[1]) and isset($arr[2])) {
				$playerpage = $arr[1];
				$user = $arr[2];
				if(!isset($arr[3])) {
					header("Location: https://new.pvp-hub.net/player/" . $user . "/stats");
					return;
				}
				if(isset($arr[4])) {
					header('Location: https://new.pvp-hub.net/player/' . $user . "/" . $arr[3]);
					return;
				}
				else {
					$prepage = $arr[3];
					if($prepage == "stats") {
						$ppage = "stats";
					}
					else if($prepage == "achievements") {
						$ppage = "achievements";
					}
					else if($prepage == "friends") {
						$ppage = "friends";
					}
					else if($prepage == "info") {
						$ppage = "info";
					}
					else if($prepage == "edit") {
						if(isset($_SESSION['uuid']) and $_SESSION['uuid'] == $user) {
							$ppage = "edit";
						}
						else {
							header("Location: https://new.pvp-hub.net/player/" . $user . "/stats");
						}
					}
					else {
						header("Location: https://new.pvp-hub.net/player/" . $user . "/stats");
						return;
					}
				}
				require('../includes/playerout.inc.php');
			}
			else {
				header('Location: https://new.pvp-hub.net/players');
			}
		}
		else {
			require("404.php");
		}
	}
	else {
		header("Location: https://new.pvp-hub.net");
	}
?>