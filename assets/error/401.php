<!DOCTYPE html>
<html style="background: url('/assets/images/401.png') fixed center no-repeat; background-size: cover;">
	<head>
		<title>401 Unauthorized - PvP-Hub.net</title>
		<meta name="author" content="Zeryther"/>
		<meta name="revisit-after" content="2 days"/>
		<meta name="description" content="PvP-Hub.net ist ein Minecraft Server Netzwerk mit vielen spa�igen Spielmodi!"/>
		<meta name="keywords" content="PvP,Hub,PvP-Hub,Minecraft,MC,.net,Zeryther,Akio,Vips"/>
		
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta charset="utf-8">
		
		<link href="/assets/css/font-awesome.css" rel="stylesheet" />
		<link href="/assets/fonts/fonts.css" rel="stylesheet"/>
		<link href="/assets/css/main.css" rel="stylesheet"/>
		<link rel="shortcut icon" href="/favicon.ico"/>
		<link rel="stylesheet" href="/assets/css/slideshow.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery.cycle.js"></script>
		<script type="text/javascript" src="/assets/js/slideshow.js"></script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-50835491-1', 'pvp-hub.net');
		  ga('send', 'pageview');
		</script>
	</head>
	<body style="background: transparent">
		<div style="text-align: center; margin-top: 20%">
			<h1 style="font-size: 62px; text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;">401</h1>
			<p style="font-size: 32px; text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;">Der Villager m&ouml;chte das Passwort von dir haben.</p>
			
			<p style="font-size: 16px; text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;">Solltest du dies f&uuml;r einen Fehler halten <a href="mailto:support@pvp-hub.net">Kontaktiere uns</a><br />Oder kehre <a href="https://www.pvp-hub.net">hier</a> zur Hauptseite zur&uuml;ck</p>
		</div>
	</body>
</html>