<?php
	require_once("online.inc.php");
	require_once("timestamp.inc.php");
	
	$fileLocation = $_SERVER['DOCUMENT_ROOT'] . ".htlastquery";
	if(file_exists($fileLocation)) {
		$file = fopen($fileLocation,"r");
		$currco = fread($file, filesize($fileLocation));
		fclose($file);
		$date = explode("//REP//", $currco);
		$date = splitTimestamp($date[0]);
		$date = new DateTime($date['year'] . "-" . $date['month'] . "-" . $date['day'] . " " . $date['hours'] . ":" . $date['minutes'] . ":" . $date['seconds']);
		$dtime = $date->getTimestamp();
		if(time() > $dtime + 60) {
			$file = fopen($fileLocation,"w");
			$content = date("Y-m-d H:i:s") . "//REP//" . implode("//REP2//", getMCOnline()) . "//REP//" . implode("//REP2//", getTSOnline());
			fwrite($file,$content);
			fclose($file);
		}
	}
	else {
		$file = fopen($fileLocation,"w");
		$content = date("Y-m-d H:i:s") . "//REP//" . implode("//REP2//", getMCOnline()) . "//REP//" . implode("//REP2//", getTSOnline());
		fwrite($file,$content);
		fclose($file);
	}
	
	
?>