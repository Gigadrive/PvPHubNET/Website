<?php
	function login($usn, $pas) {
		if(isset($usn) and isset($pas)) {
			require_once("mysql.inc.php");
			require_once("string_validator.inc.php");
			require_once("crypt.inc.php");
			
			$table = $mysql_utable;
			
			//Validating Vars
			if(!isValidUsn($usn) or !isValidPwlg($pas)) {
				mysql_close($con);
				return "Username or Password is invalid";
			}
			
			//Querying
			$statement = "SELECT * FROM `$table` WHERE BINARY lastname='$usn'";
			$result = mysql_query($statement);
			
			if(!$result) {
				mysql_close($con);
				return "Failed to Query MySQL";
			}
			
			//Writing Vars
			while($row = mysql_fetch_object($result)) {
				$md5pass = $row->password;
				$salt = $row->salt;
			}	
		
			//Evaluation
			if(isset($md5pass) and isset($salt)) {
				$state = validate($md5pass, $pas, $salt);
				if($state) {
					mysql_close($con);
					return "User succesfully logged in";
				}
				else {
					mysql_close($con);
					return "Invalid Password";
				}
			}
			else {
				mysql_close($con);
				return "User does not exist";
			}
		}
		else {
			mysql_close($con);
			return "Username or Password is not set";
		}
	}
?>