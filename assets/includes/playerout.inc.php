							<?php require_once "top.inc.php"; 
							require_once("uuid.inc.php");
							require_once("string_validator.inc.php");?>
							<section id="player-profile">
							<?php
								error_reporting(E_ALL);
								ini_set("display_errors", "on");
								ini_set("display_startup_errors", "on");
							if(!isset($user) or !isset($ppage)) {
								return;
							}
							
							function secondsToTime($inputSeconds) {
								$secondsInAMinute = 60;
								$secondsInAnHour  = 60 * $secondsInAMinute;
								$secondsInADay    = 24 * $secondsInAnHour;

								// extract days
								$days = floor($inputSeconds / $secondsInADay);

								// extract hours
								$hourSeconds = $inputSeconds % $secondsInADay;
								$hours = floor($hourSeconds / $secondsInAnHour);

								// extract minutes
								$minuteSeconds = $hourSeconds % $secondsInAnHour;
								$minutes = floor($minuteSeconds / $secondsInAMinute);

								// extract the remaining seconds
								$remainingSeconds = $minuteSeconds % $secondsInAMinute;
								$seconds = ceil($remainingSeconds);

								// return the final array
								$obj = array(
									'd' => (int) $days,
									'h' => (int) $hours,
									'm' => (int) $minutes,
									's' => (int) $seconds,
								);
								return $obj;
							}
							

								$len = strlen($user);
								if($len == 32) {
									if(isAlphaNumeric($user)) {
										$sql_pdata = mysql_query("SELECT * FROM users WHERE mojangid='" . $user . "'");
										if(mysql_num_rows($sql_pdata) != 1){
											$isuser = false;
										}
										else {
											$isuser = true;
										}
									}
									else {
										$isuser = false;
									}
								}
								else if($len > 0 and $len <= 16) {
									if(isAlphaNumeric($user, "_")) {
										$sql_pdata = mysql_query("SELECT * FROM users WHERE lastname='" . $user . "'");
										if(mysql_num_rows($sql_pdata) != 1) {
											$response = getUUID($user);
											if($response) {
												$profile = $response->{'profiles'};
												if($profile != null) {
													$iiuuid = $response->{'profiles'}[0]->{'id'};
													$mineusn = $response->{'profiles'}[0]->{'name'};
													$sql_pdata = mysql_query("SELECT * FROM users WHERE mojangid='" . $iiuuid . "'");
													if(mysql_num_rows($sql_pdata) != 1){
														$isuser = false;
													}
													else {
														header('Location: https://www.pvp-hub.net/player/' . $iiuuid);
													}
												}
												else {
													$isuser = false;
												}
											}
											else {
												$isuser = false;
											}
										}
										else {
											while($row = mysql_fetch_array($sql_pdata)) {
												$uuid = $row['mojangid'];
												header('Location: https://www.pvp-hub.net/player/' . $uuid);
											}
										}
									}
									else {
										$isuser = false;
									}
								}
								else {
									$isuser = false;
								}
							
							if($isuser) {
								while($row=mysql_fetch_array($sql_pdata)){
									$username = $row["lastname"];
									$descr = $row["description"];
									$rank_id = $row["rank"];
									$uid = $row["id"];
									$uuid = $row["mojangid"];
									$achieved_a = $row["achievements"];
									$friends_a = $row["friends"];
									$language = $row["language"];
									$coins = $row["coins"];
									$chips = $row["chips"];
									$playedtime = $row["playedtime"];
									$premiumend = $row["premiumend"];
									$firstjoin = $row["joined"];
									$facebook = $row["facebook"];
									$twitter = $row["twitter"];
									$youtube = $row["youtube"];
								}
								if(!isset($mineusn)) {
									$mineusn = getSession($uuid)->{'name'};
								}
								
								if($mineusn != $username) {
									$knownas = "Zuletzt gesehen als <b>" . $username . "</b>";
									$knu = $username;
									$username = $mineusn;
								}
								else {
									$knownas = "";
								}
								
								$played = secondsToTime($playedtime);
								$pstring = "";
								$btn = array();
								$last = "";
								$forelast = "";
								
								if($played['d'] > 0) {
									$last = "d";
								}
								if($played['h'] > 0) { 
									$forelast = $last;
									$last = "h";
								}
								if($played['m'] > 0) { 
									$forelast = $last;
									$last = "m";
								}
								if($played['s'] > 0) { 
									$forelast = $last;
									$last = "s";
								}
								
								
								if($played['d'] > 0) {
									if($played['d'] == 1) {
										$pstring .= "<b>" . $played['d'] . "</b> Tag";
									}
									else {
										$pstring .= "<b>" . $played['d'] . "</b> Tage";
									}
									
									if($last != "d" and $forelast != "d") {
										$pstring .= ", ";
									}
									else if($forelast == "d") {
										$pstring .= " und ";
									}
									else if($last == "d") {
										$pstring .= " ";
									}
								}
								if($played['h'] > 0) {
									if($played['h'] == 1) {
										$pstring .= "<b>" . $played['h'] . "</b> Stunde";
									}
									else {
										$pstring .= "<b>" . $played['h'] . "</b> Stunden";
									}
									
									if($last != "h" and $forelast != "h") {
										$pstring .= ", ";
									}
									else if($forelast == "h") {
										$pstring .= " und ";
									}
									else if($last == "h") {
										$pstring .= " ";
									}
								}
								if($played['m'] > 0) {
									if($played['m'] == 1) {
										$pstring .= "<b>" . $played['m'] . "</b> Minute";
									}
									else {
										$pstring .= "<b>" . $played['m'] . "</b> Minuten";
									}
									
									if($last != "m" and $forelast != "m") {
										$pstring .= ", ";
									}
									else if($forelast == "m") {
										$pstring .= " und ";
									}
									else if($last == "m") {
										$pstring .= " ";
									}
								}
								if($played['s'] > 0) {
									if($played['s'] == 1) {
										$pstring .= "<b>" . $played['s'] . "</b> Sekunde";
									}
									else {
										$pstring .= "<b>" . $played['s'] . "</b> Sekunden";
									}
									
									if($last != "s" and $forelast != "s") {
										$pstring .= ", ";
									}
									else if($forelast == "s") {
										$pstring .= " und ";
									}
									else if($last == "s") {
										$pstring .= " ";
									}
								}
								
								if($pstring == "") {
									$pstring .= "Keine ";
								}
							}
							else {
								$username = "Unbekannter Nutzer";
								$knownas = "";
							}
							
							?>
							<!-- Start Second Head -->
							<title><?php print $username; if($knownas != "") { print " (" . $knu . ")"; } ?> - PvP-Hub.net</title>
							<!-- End Second Head -->
			    			<div id="psummary">
			    				<div class="pageWidth">
									<table width="100%" border="0">
										<tr>
											<td width="25%" valign="top">
												<?php if($isuser) { ?>
												<center><img src="https://www.pvp-hub.net/assets/avatar/index.php?name=<?php print $username; ?>" alt="" width="107"/></center>
												<?php include("player_badge.inc.php"); ?>
												<?php } else { ?>
												<center><img src="https://www.pvp-hub.net/assets/avatar/index.php?name=PvPHubMC" alt="" width="107"/></center>
												<?php } ?>
											</td>
											<td width="75%" valign="top">
												<div style="width: 880px; margin: 35px 0px 0px; height: 135px;">
												<h1 style="color: #FFF; font-size: 42px; margin-bottom: 0px; margin-left: 40px;">  <?php print $username;?>
												<?php if($isuser) { ?>
												<ul style="margin: 0px 0px -2px -40px; display: inline-block; font-size: small;">
														<li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; color: #FFF; float: left; border-radius: 3px;"><?php print $knownas; ?></li>
														<a href="/player/<?php print $uuid; ?>/stats"><li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															Statistiken
														</li></a>
														<a href="/player/<?php print $uuid; ?>/achievements"><li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															Achievements
														</li></a>
														<a href="/player/<?php print $uuid; ?>/friends"><li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															Freunde
														</li></a>
														<a href="/player/<?php print $uuid; ?>/info"><li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															Info
														</li></a>
														<?php if(isset($_SESSION['uuid']) and $_SESSION['uuid'] == $uuid) { ?>
														<a href="/player/<?php print $uuid; ?>/edit"><li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 11px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															Profil bearbeiten
														</li></a>
														<?php } ?>
												</ul><?php } ?></h1>
													<?php if($isuser) { ?>
													<ul style="margin: 15px 0px 0px; float: left; width: 800px;">
														<li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 15px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															<i class="fa fa-money"></i> <b><?php print $coins; ?></b> Coins
														</li>
														<li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 15px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
															<i class="fa fa-ticket"></i> <b><?php print $chips; ?></b> Chips
														</li>
														<li style="padding: 7px 10px; margin: 0px 10px 0px 0px; font-size: 15px; display: inline; background: none repeat scroll 0% 0% #154760; text-shadow: 0px 1px 0px #10384C; color: #FFF; float: left; border-radius: 3px;">
														<i class="fa fa-clock-o"></i> <?php print $pstring; ?>Spielzeit
														</li>
													</ul>
													<?php } ?>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
							<?php if($isuser) { ?>
							<div class="pageWidth">
								<?php
									if($ppage == "stats"){
										require_once "/home/apache2/www/assets/includes/profile_stats.inc.php";
									} else if($ppage == "achievements"){
										require_once "/home/apache2/www/assets/includes/profile_achievements.inc.php";
									} else if($ppage == "friends"){
										require_once "/home/apache2/www/assets/includes/profile_friends.inc.php";
									} else if($ppage == "info"){
										require_once "/home/apache2/www/assets/includes/profile_info.inc.php";
									} else if($ppage == "edit"){
										require_once "/home/apache2/www/assets/includes/profile_edit.inc.php";
									} else {
										require_once "/home/apache2/www/assets/includes/profile_stats.inc.php";
									}
								
								?>
							</div>
							<?php } ?>
</section>			    			
<?php include "bottom.inc.php"; ?>