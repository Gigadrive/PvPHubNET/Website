<?php
	function lockWithSalt($string, $salt) {
		$md5_1 = md5($string);
		$lstring = md5(md5(md5(md5(md5(md5($md5_1 . $salt))))));
		return $lstring;
	}
	
	function generateSalt($max = 15) {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $i = 0;
        $salt = "";
        while ($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }
        return $salt;
	}
	
	function generateToken($max = 65) {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $i = 0;
        $salt = "";
        while ($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }
        return $salt;
	}
	
	function validate($dbpass, $inpass, $salt, $premium) {
		if(lockWithSalt($inpass, $salt) == $dbpass) {
			if($premium == 1) {
				return "Good";
			}
			else {
				return "No Premium";
			}
		}
		return "Bad Login";
	}
?>