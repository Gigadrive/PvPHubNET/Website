<?php 
session_start();
require("mysql.inc.php"); 
require("query.inc.php");
require("SwordPing.php");

if (strpos($_SERVER["PHP_SELF"],'forum') !== false) {
    if(!isset($_SESSION["id"])){
        header("Location: /login");
    }
}

if (strpos($_SERVER["PHP_SELF"],'settings') !== false) {
    if(!isset($_SESSION["id"])){
        header("Location: /login");
    }
}

$file = fopen($fileLocation,"r");
$currco = fread($file, filesize($fileLocation));
fclose($file);
$res = explode("//REP//", $currco);
$date = splitTimestamp($res[0]);
$mc = explode("//REP2//", $res[1]);
$ts = explode("//REP2//", $res[2]);
if($date['minutes'] + 2 > 59) {
$hours = $date['hours'] + 1;
$minutes = $date['minutes'] + 2 - 60;
}
else {
$hours = $date['hours'];
$minutes = $date['minutes'] + 2;
}

if($minutes < 10) {
	$minutes = 0 . $minutes;
}
$fdate = $hours . ":" . $minutes . ":" . $date['seconds'];

function getNameFromID($id){
    $sql = mysql_query("SELECT * FROM users WHERE id='" . $id . "'");
    while($row = mysql_fetch_array($sql)){
        $name = $row["name"];
    }
    
    return $name;
}

function getSignatureFromID($id){
    $sql = mysql_query("SELECT * FROM users WHERE id='" . $id . "'");
    while($row = mysql_fetch_array($sql)){
        $signature = $row["signature"];
    }
    
    return $signature;
}

function getRankFromName($name){
    $sql = mysql_query("SELECT * FROM users WHERE name='" . $name . "'");
    while($row = mysql_fetch_array($sql)){
        $rank = $row["rank"];
    }
    
    if($rank == 0){
            $badge = '<p id="urank" class="r0">Spieler</p>';
    } else if($rank == 1){
            $badge = '<p id="urank" class="r1">Premium Spieler</p>';
    } else if($rank == 2){
            $badge = '<p id="urank" class="r1">Life-Time-Premium Spieler</p>';
    } else if($rank == 3){
            $badge = '<p id="urank" class="r3">VIP</p>';
    } else if($rank == 4){
            $badge = '<p id="urank" class="r3">Youtuber</p>';
    } else if($rank == 5){
            $badge = '<p id="urank" class="r4">Beta Tester</p>';
    } else if($rank == 6){
            $badge = '<p id="urank" class="r6">Bau-Team</p>';
    } else if($rank == 7){
            $badge = '<p id="urank" class="r5">Server-Team</p>';
    } else if($rank == 8){
            $badge = '<p id="urank" class="r7">Moderator</p>';
    } else if($rank == 9){
            $badge = '<p id="urank" class="r8">Developer</p>';
    } else if($rank == 10){
            $badge = '<p id="urank" class="r10">Admin</p>';
    } else if($rank == 11){
            $badge = '<p id="urank" class="r10">Owner</p>';
    } else {
            $badge = '<p id="urank" class="r0">Spieler</p>';
    }
    
    return $badge;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			$phpself = $_SERVER['PHP_SELF'];
			$main = false;
			$news = false;
			$community = false;
			$pname = "";
			$gamemodes = false;
			$shop = false;
			$support = false;
			
			$mconline = false;
			$tsonline = false;
			
			if($mc[0] == "online") {
				$mconline = true;
			}
			if($ts[0] != "offline") {
				$tsonline = true;
			}
			
			if($phpself == "/index.php" || $phpself == "/") {
				$main = true;
				$page = "Home";
			}
			else if($_SERVER["PHP_SELF"] == "/forum/") {
				$page = "Forum";
			}
			else if($phpself == "/news/index.php" || $phpself == "/article/index.php" || $phpself == "/event/index.php") {
				$news = true;
				$page = "News";
			}
			else if($phpself == "/players/index.php") {
				$community = true;
				$page = "Community";
			}
			else if($phpself == "/gametypes/index.php" || $phpself == "/gametypes/hunger-games/index.php" || $phpself == "/gametypes/hunger-games/maps/index.php" || $phpself == "/gametypes/hunger-games/toplist/index.php" || $phpself == "/gametypes/infection-wars/index.php" || $phpself == "/gametypes/infection-wars/maps/index.php" || $phpself == "/gametypes/infection-wars/toplist/index.php" || $phpself == "/gametypes/deathmatch/index.php" || $phpself == "/gametypes/deathmatch/maps/index.php" || $phpself == "/gametypes/deathmatch/toplist/index.php" || $phpself == "/gametypes/souppvp/index.php" || $phpself == "/gametypes/souppvp/maps/index.php" || $phpself == "/gametypes/souppvp/toplist/index.php") {
				$gamemodes = true;
				$page = "Spielmodi";
			}
			else if($phpself == "/store/index.php") {
				$shop = true;
				$page = "Shop";
			}
			else if($phpself == "/support/index.php") {
				$support = true;
				$page = "Support";
			}
			else if($phpself == "/support/ticket/new/index.php") {
				$support = true;
				$page = "Ticket erstellen";
			}
			else if($phpself == "/support/commands/index.php" || $phpself == "/support/commands/hub/index.php" || $phpself == "/support/commands/report/index.php" || $phpself == "/support/commands/coins/index.php" || $phpself == "/support/commands/chips/index.php") {
				$support = true;
				$page = "Befehle";
			}
			else if($phpself == "/imprint/index.php") {
				$support = true;
				$page = "Impressum";
			}
			else if($phpself == "/support/terms-of-use/index.php" || $phpself == "/support/terms-of-use/minecraft-server/index.php" || $phpself == "/terms-of-use/ts-server/index.php" || $phpself == "/support/terms-of-use/store/index.php" || $phpself == "/support/terms-of-use/ts-server/index.php") {
				$support = true;
				$page = "Nutzungsbedingungen";
			}
			else if($phpself == "/privacypolicy/index.php") {
				$support = true;
				$page = "Datenschutzerklärung";
			}
			else if($phpself == "/disclaimer/index.php") {
				$support = true;
				$page = "Haftungsausschluss";
			}
			else if($phpself == "/server/index.php") {
				$page = "Server";
			}
			else if($phpself == "/login/index.php") {
				$page = "Login";
			}
                        else if($phpself == "/forum/index.php") {
				$page = "Forum";
			}
                        else if($phpself == "/forum/category/index.php") {
				$page = "Kategorie ansehen";
			}
                        else if($phpself == "/forum/topic/index.php") {
				$page = "Thread ansehen";
			}
                        else if($phpself == "/forum/newthread/index.php") {
				$page = "Neues Thema erstellen";
			}
            else if($phpself == "/pixelbit/index.php") {
				$page = "Team PixelBit";
			}
			else if(isset($_SERVER['REQUEST_URI'])) {
				if(substr($ru, 0, 8) != "/player/") {
					$page = "Undefined";
				}
				else {
					$community = true;
					$page = "";
				}
			}
			else {
				$page = "Undefined";
			}
		if($page != "") {
?>
		<title><?php echo($page . " - PvP-Hub.net"); ?></title>
<?php
		} 
?>
		<meta name="author" content="Zeryther"/>
		<meta name="revisit-after" content="2 days"/>
		<meta name="description" content="PvP-Hub.net ist ein Minecraft Server Netzwerk mit vielen spaßigen Spielmodi!"/>
		<meta name="keywords" content="PvP,Hub,PvP-Hub,Minecraft,MC,.net,Zeryther,Akio,Vips"/>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<link href="/assets/css/font-awesome.css" rel="stylesheet" />
		<link href="/assets/fonts/fonts.css" rel="stylesheet"/>
		<link href="/assets/css/main.css" rel="stylesheet"/>
		<link rel="shortcut icon" href="/favicon.ico"/>
		<link rel="stylesheet" href="/assets/css/slideshow.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery.cycle.js"></script>
		<script type="text/javascript" src="/assets/js/slideshow.js"></script>
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		
		<script src="/assets/js/tinymce/tinymce.min.js"></script>
		<script>
        	tinymce.init({selector:'textarea'});
		</script>

		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-50835491-1', 'pvp-hub.net');
		  ga('send', 'pageview');
		
		</script>
		
		 <script>
			$(function() {
			$( document ).tooltip();
			});
		 </script>
	</head>
	
	<body>
		<div id="main-content">
			<div id="header">
				<section id="topbar">
					<div class="pageWidth">
						<?php if(isset($_SESSION["username"])){ ?>
							<p>
								<?php if($_SESSION["rank"] == 9 || $_SESSION["rank"] == 10 || $_SESSION["rank"] == 11){ print '<a href="http://pvp-hub.net/admin">Administration</a> - '; } ?><a href="/player/?u=<?php print $_SESSION["username"]; ?>">Mein Profil</a> - <a href="/settings">Einstellungen</a> - <a href="/logout">Abmelden</a>
							</p>
							<div class="user">
								<span>
									<img src="https://a.vatar.co/a/<?php print $_SESSION['username']; ?>/32.png"/>
								</span>
								<p>
									Eingeloggt als <a href="/player/?u=<?php print $_SESSION['username']; ?>"><?php print $_SESSION['username']; ?></a>
								</p>
							</div>
						<?php } else { ?>
							<p>&nbsp;</p>
							<div class="user">
								<span>
									<img src="https://minotar.net/helm/Steve/32.png"/>
								</span>
								<p>
									<a href="/login">Login &raquo;</a>
								</p>
							</div>
						<?php } ?>
					</div>
				</section>
				<noscript>
					<section id="topbar" style="background-color: #FF0000 !important;">
						<div class="pageWidth">
							<center>
								<h1 style="font-family: Arial, Helvetica, sans-serif; color: #FFFFFF; margin: 14px;">Bitte aktivire JavaScript um PvP-Hub.net richtig nutzen zu k&ouml;nnen!</h1>
							</center>
						</div>
					</section>
				</noscript>
				<div id="colorOverlay">
					<div class="pageWidth">
						<div class="topRight">
                            <!--<div class="topRightAnno">
                                <p>NEU: <i><a href="/pixelbit">Team PixelBit &raquo;</a></i></p>
                            </div>-->
                            <?php
                                                    $query = new SwordPing("pvp-hub.net", 25565);
                                                    ?>
                            <div id="server-online" style="margin-top: 15px; margin-right: -90px">
                                                        <span>
                                                            Server-IP: pvp-hub.net<br/>
                                                            Spieler: <span style="color: #37BA00"><?php print $query->getOnlinePlayers(); ?> / <?php print $query->getMaxPlayers(); ?></span>
                                                        </span>
                                                    </div>
                        </div>
						<div>
							<a href="/">
								<img src="/assets/images/logo.png" width="180"/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php 
    
        		$active_link = ' class="active"';
				$active_link_sub = ' class="active has-sub"';
				$link_sub = ' class="has-sub"';
    
    		?>
			<div id='cssmenu'>
				<div class="pageWidth">
			        <ul>
						<li<?php if($main){ print $active_link; }?>><a href='/'><span>Home</span></a></li>
						<?php /*<li <?php if($forum){ print $active_link; }?>><a style="cursor: pointer" onclick="forumAlert()"><span>Forum</span></a></li> */ ?>
						<?php /*<li<?php if($news){ print $active_link; }?>><a href='/news'><span>News</span></a></li> */ ?>
						<li<?php if($community){ print $active_link_sub; } else { print $link_sub; }?>><a href='/players'><span>Community <i class="fa fa-caret-down"></i></span></a>
							<ul>
								<li><a href="/forum"><span>Forum</span></a>
								<li><a href="/players"><span>Spieler-Verzeichnis</span></a>
							</ul>
						</li>
						<li<?php if($gamemodes){ print $active_link; }?>><a href='/gametypes'><span>Spielmodi</span></a></li>
						<li<?php if($shop){ print $active_link; }?>><a href="http://store.pvp-hub.net" target="_blank">Shop</a></li>
						<li<?php if($support){ print $active_link; }?>><a href='/support'><span>Support</span></a></li>
			        </ul>
		        </div>
		    </div>