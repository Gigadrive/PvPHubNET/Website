<?php
	error_reporting(E_ALL);
	ini_set("display_errors", "on");
	ini_set("display_startup_errors", "on");
	
	function startsWith($haystack, $needle) {
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	function endsWith($haystack, $needle) {
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
	
	function replacets3($string) {
		$string = str_replace("\s", " ", $string);
		$string = str_replace("°", "�", $string);
		$string = str_replace("§", "�", $string);
		$string = str_replace("Ä", "�", $string);
		$string = str_replace("Ö", "�", $string);
		$string = str_replace("Ü", "�", $string);
		$string = str_replace("´", "�", $string);
		$string = str_replace("²", "�", $string);
		$string = str_replace("³", "�", $string);
		$string = str_replace("€", "�", $string);
		
		return $string;
	}
	
	function getMCOnline() {
		require_once("status.class.php");
		$status = new MinecraftServerStatus();
		
		$response = $status->getStatus('pvp-hub.net');
		
		if(!$response) {
			return array(
				"status" => "offline"
			);
		}
		else {
			return array(
				"status" => "online",
				"hostname" => $response['hostname'],
				"version" => $response['version'],
				"protocol" => $response['protocol'],
				"maxplayers" => $response['maxplayers'],
				"motd" => $response['motd'],
				"motd_raw" => $response['motd_raw'],
				"favicon" => $response['favicon'],
				"ping" => $response['ping']
			);
		}
	}
	
	function getTSOnline() {
		$fp = fsockopen("pvp-hub.net", 10011, $errno, $errstr, 3);
		if(!$fp) {
			return array(
				"status" => "offline",
				"errno" => $errno,
				"errstr" => $errstr
			);
		}
		else {
			fputs($fp, "use 1\r\n");
			fputs($fp, "login serveradmin ichbindeinvater32dotcom\r\n");
			fputs($fp, "channellist\r\n");
			fputs($fp, "clientlist -away -voice -groups\r\n");
			fputs($fp, "logout\r\n");
			fputs($fp, "quit\r\n");
			$buffer = array();
			$i = 0;
			while(!feof($fp)) {
				$buffer[$i] = fgets($fp);
				$i++;
			}
			fclose($fp);
			
			$craw = explode(" ", $buffer[4]);
			$uraw = explode(" ", $buffer[6]);
			
			$channels = array();
			$lastarray = array();
			$lasti = 0;
			$channeli = 0;
			
			foreach($craw as $channel) {
				if(startsWith($channel, "pid=")) {
					$channels[$channeli] = $lastarray;
					$channeli++;
					$lastarray = array();
					$lastarray[0] = $channel;
					$lasti = 1;
				}
				else {
					$lastarray[$lasti] = $channel;
					$lasti++;
				}
			}
			
			$realchannels = array();
			$reali = 0;
			
			foreach($channels as $channel) {
				if($channel != $channels[0]) {
					$realch = array();
					for($i = 0; $i < 5; $i++) {
						$split = explode("=", $channel[$i]);
						$realch[$split[0]] = replacets3($split[1]);
					}	
					$realchannels[$reali] = $realch;
					$reali++;
				}
			}
			
			print_r($realchannels);
			$final = array("channels" => $realchannels);
			
			foreach($uraw as $user) {
				echo($user . "<br />");
			}
		}
	}
	
	//print_r(getMCOnline());
	getTSOnline();
?>