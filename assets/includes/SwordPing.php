<?php

class SwordPing
{

	private $Info = false;
	private $New = true;
	private $OnlinePlayers = 0;
	private $MaxPlayers = 0;
	private $Version = 0;
	private $Description = null;
	private $Online = false;

	public function __construct($IP, $Port = 25565, $Timeout = 2)
	{
		require "MinecraftServerPing.php";
		$Query = null;
		try {
			$Query = new MinecraftPing($IP, $Port, $Timeout);
			$this->Info = $Query->Query();
			if ($this->Info === false) {
				$Query->Close();
				$Query->Connect();

				$this->Info = $Query->QueryOldPre17();
				$this->New = false;
			}
		} catch (MinecraftPingException $e) {
			$this->Online = false;
		}

		if ($Query !== null) {
			$Query->Close();
		}

		if ($this->New) {
			$this->OnlinePlayers = $this->Info['players']['online'];
			$this->MaxPlayers = $this->Info['players']['max'];
			$this->Version = $this->Info['version']['name'];
			$this->Description = $this->Description['description']['text'];
		} else {
			$this->OnlinePlayers = $this->Info['Players'];
			$this->MaxPlayers = $this->Info['MaxPlayers'];
			$this->Version = $this->Info['Version'];
		}
		$this->Online = true;
	}

	/**
	 * Returns true if 1.7 or newer.
	 * Returns false if 1.6 or older.
	 */
	public function getIsNew()
	{
		return $this->New;
	}

	/**
	 * Returns current online players in int format.
	 */
	public function getOnlinePlayers()
	{
		return $this->OnlinePlayers;
	}

	/**
	 * Returns max players in int format.
	 */
	public function getMaxPlayers()
	{
		return $this->MaxPlayers;
	}

	/**
	 * Return version in string version.
	 */
	public function getVersion()
	{
		return $this->Version;
	}

	/**
	 * Returns MOTD (only for 1.7+)
	 * Returns null if non-1.7.
	 */
	public function getDescription()
	{
		return $this->Description;
	}

	/**
	 * Returns if server pinged is online
	 */
	public function isOnline()
	{
		return $this->Online;
	}


}