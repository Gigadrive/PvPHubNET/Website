<?php
	function isValidUsn($string) {
		$regex = "/^[A-Za-z0-9_]{5,16}+$/";
		
		return preg_match($regex, $string);
	}
	
	function isValidPw($string) {
		$regex = "/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/";
		$regex1 = "/^[A-Za-z0-9_.?!\+=-]+$/";
		
		if(preg_match($regex, $string) and preg_match($regex1, $string)) {
			return true;
		}
		return false;
	}
	
	function isValidPwlg($string) {
		$regex = "/^[A-Za-z0-9_.?!\+=-]{8,}+$/";
		return preg_match($regex, $string);
	}
	
	function isValidEmail($string) {
		$regex = "/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/";
		return preg_match($regex, $string);
	}
	
	function isAlphaNumeric($string, $specialchars = "") {
		$regex = "/^[A-Za-z0-9" . $specialchars . "]+$/";
		return preg_match($regex, $string);
	}
?>