<div class="pageWidth">
	<table width="100%" border="0">
		<tr>
			<td width="50%" valign="top">
				<!-- LEFT -->
				<h1 class="sectionTitle">Profil-Infos</h1>
				<div class="widget">
					<div class="subHeading">Grundlegende Informationen</div>
					<div style="margin: 10px; font-size: 13px;">
						<table width="100%" border="0">
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Username</p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $username; ?></p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">UUID <a href="#" title='"Universally Unique Identifier" ist der Identifikationsersatz f&uuml;r den Namen, anhand ihm kann man sich bald in Minecraft umbenennen und trotzdem noch von Servern gefunden werden.'>(?)</a></p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $uuid; ?></p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">UID <a href="#" title='"User Identifier" ist zur schnelleren Wiederfindung eines Nutzers in unserem System gedacht.'>(?)</a></p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $uid; ?></p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Benutzer Beschreibung</p>
								</td>
								<td width="50%" valign="top">
									<?php
									
									if($descr == ""){
										print '<p>Keine Beschreibung verf&uuml;gbar</p>';
									} else {
										print '<p>' . $descr . '</p>';
									} ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<?php if(!($youtube == "") and !($facebook == "") and !($twitter == "")){ ?>
					
				<?php }?>
				<div class="widget">
					<div class="subHeading">Kontaktm&ouml;glichkeiten</div>
					<div style="margin: 10px; font-size: 13px;">
						<table width="100%" border="0">
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc"><i class="fa fa-facebook-square"></i> Facebook</p>
								</td>
								<td width="50%" valign="top">
									<p>
										<?php if($facebook == ""){
											print "Nicht angegeben";
										} else {
											print '<a href="https://www.facebook.com/' . $facebook . '" target="_blank">' . $facebook . '</a>';
										}
										?>
									</p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc"><i class="fa fa-twitter-square"></i> Twitter</p>
								</td>
								<td width="50%" valign="top">
									<p>
										<?php if($twitter == ""){
											print "Nicht angegeben";
										} else {
											print '<a href="https://www.twitter.com/' . $twitter . '" target="_blank">' . $twitter . '</a>';
										}
										?>
									</p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc"><i class="fa fa-youtube-play"></i> YouTube</p>
								</td>
								<td width="50%" valign="top">
									<p>
										<?php if($youtube == ""){
											print "Nicht angegeben";
										} else {
											print '<a href="https://www.youtube.com/' . $youtube . '" target="_blank">' . $youtube . '</a>';
										}
										?>
									</p>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="widget">
					<div class="subHeading">PvP-Hub Informationen</div>
					<div style="margin: 10px; font-size: 13px;">
						<table width="100%" border="0">
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Coins</p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $coins; ?></p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Chips</p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $chips; ?></p>
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Spielzeit</p>
								</td>
								<td width="50%" valign="top">
									<?php
									
									$pstring_unfo = str_replace("<b>", "", $pstring);
									$pstring_unfo = str_replace("</b>", "", $pstring_unfo);
									
									print '<p>' . $pstring_unfo . '</p>';
									
									?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
			<td width="50%" valign="top">
				<!-- RIGHT -->
                <?php

                    $topics_started_sql = mysql_query("SELECT * FROM forum_topic WHERE first_post_by = '" . $uid . "'");

                    $topics_started = mysql_num_rows($topics_started_sql);

                    $posts_created_sql = mysql_query("SELECT * FROM forum_posts WHERE author = '" . $uid . "'");
                    $posts_created = mysql_num_rows($posts_created_sql);

                ?>
				<h1 class="sectionTitle">Forum-Infos</h1>
                <div class="widget">
					<div class="subHeading">Beitr&auml;ge</div>
					<div style="margin: 10px; font-size: 13px;">
						<table width="100%" border="0">
							<tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Gestartete Themen</p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $topics_started; ?></p>
								</td>
							</tr>
                            <tr>
								<td width="50%" valign="top">
									<p class="infoDesc">Geschriebene Beitr&auml;ge</p>
								</td>
								<td width="50%" valign="top">
									<p><?php print $posts_created; ?></p>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>