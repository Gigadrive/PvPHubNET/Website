<?php
	function startsWith($haystack, $needle) {
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	function endsWith($haystack, $needle) {
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
	
	function replacets3($string) {
		$string = str_replace("\s", " ", $string);
		$string = str_replace("°", "�", $string);
		$string = str_replace("§", "�", $string);
		$string = str_replace("Ä", "�", $string);
		$string = str_replace("Ö", "�", $string);
		$string = str_replace("Ü", "�", $string);
		$string = str_replace("´", "�", $string);
		$string = str_replace("²", "�", $string);
		$string = str_replace("³", "�", $string);
		$string = str_replace("€", "�", $string);
		$string = str_replace("\p", "|", $string);
		$string = str_replace("»", "�", $string);
		$string = str_replace("►", "", $string);
		$string = str_replace("◄", "", $string);
		$string = str_replace("ä", "�", $string);
		$string = str_replace("ö", "�", $string);
		$string = str_replace("ü", "�", $string);
		
		return $string;
	}
	
	function getMCOnline($host = "pvp-hub.net", $version = "1.7.*", $port = 63394) {
		require_once("status.class.php");
		$status = new MinecraftServerStatus(1);
		
		$response = $status->getStatus($host, $version, $port);
		
		if(!$response) {
			return array(
				"status" => "offline"
			);
		}
		else {
			return array(
				"status" => "online",
				"hostname" => $response['hostname'],
				"version" => $response['version'],
				"protocol" => $response['protocol'],
				"maxplayers" => $response['maxplayers'],
				"motd" => replacets3($response['motd']),
				"motd_raw" => replacets3($response['motd_raw']),
				"favicon" => $response['favicon'],
				"ping" => $response['ping'],
				"players" => $response['players']
			);
		}
	}
	
	function getTSOnline() {
		$fp = fsockopen("pvp-hub.net", 10011, $errno, $errstr, 3);
		if(!$fp) {
			return array(
				"status" => "offline",
				"errno" => $errno,
				"errstr" => $errstr
			);
		}
		else {
			fputs($fp, "use 1\r\n");
			fputs($fp, "login serveradmin ichbindeinvater32dotcom\r\n");
			fputs($fp, "hostinfo\r\n");
			fputs($fp, "logout\r\n");
			fputs($fp, "quit\r\n");
			$buffer = array();
			$i = 0;
			while(!feof($fp)) {
				$buffer[$i] = fgets($fp);
				$i++;
			}
			fclose($fp);
			
			$info = $buffer[4];
			$iarr = explode(" ", $info);
			$finalinfo = array();
			foreach($iarr as $if) {
				$ifarr = explode("=", $if);
				$finalinfo[$ifarr[0]] = $ifarr[1];
			}
			return $finalinfo;
		}
	}
?>