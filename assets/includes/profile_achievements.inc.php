<h1 class="sectionTitle">Global / Hub</h1>
<?php

$sql_g = mysql_query("SELECT * FROM achievements WHERE category='global'");
while($row_g = mysql_fetch_array($sql_g)){
    $aid = $row_g["id"];
    $aname = $row_g["name"];
    $adescr = $row_g["description"];
    $aarray = $row_g["achieved_by"];
    $aimg = $row_g["img"];
    
    $adescrNew = str_replace("ü", "&uuml;", $adescr);
    /*$adescrNew = str_replace("ö", "&ouml;", $adescrNew);
    $adescrNew = str_replace("ü", "&uuml;", $adescrNew);
    $adescrNew = str_replace("Ä", "&Auml;", $adescrNew);
    $adescrNew = str_replace("Ö", "&Ouml;", $adescrNew);
    $adescrNew = str_replace("Ü", "&Uuml;", $adescrNew);
    $adescrNew = str_replace("ß", "&szlig;", $adescrNew);*/
    
    if($aimg == 0){
        $aimg = "0";
    } else {
        $aimg = $aid;
    }
    
    $aarray = explode(",", $aarray);
    
    if(in_array($id, $aarray)){
        echo '<li class="unlocked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    } else {
        echo '<li class="locked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    }
}

?>
<h1 class="sectionTitle">Survival Games</h1>
<?php

$sql_g = mysql_query("SELECT * FROM achievements WHERE category='survival_games'");
while($row_g = mysql_fetch_array($sql_g)){
    $aid = $row_g["id"];
    $aname = $row_g["name"];
    $adescr = $row_g["description"];
    $aarray = $row_g["achieved_by"];
    $aimg = $row_g["img"];
    
    $adescrNew = str_replace("ä", "&auml;", $adescr);
    $adescrNew = str_replace("ö", "&ouml;", $adescrNew);
    $adescrNew = str_replace("ü", "&uuml;", $adescrNew);
    $adescrNew = str_replace("Ä", "&Auml;", $adescrNew);
    $adescrNew = str_replace("Ö", "&Ouml;", $adescrNew);
    $adescrNew = str_replace("Ü", "&Uuml;", $adescrNew);
    $adescrNew = str_replace("ß", "&szlig;", $adescrNew);
    
    if($aimg == 0){
        $aimg = "0";
    } else {
        $aimg = $aid;
    }
    
    $aarray = explode(",", $aarray);
    
    if(in_array($id, $aarray)){
        echo '<li class="unlocked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    } else {
        echo '<li class="locked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    }
}

?>
<h1 class="sectionTitle">Infection Wars</h1>
<?php

$sql_g = mysql_query("SELECT * FROM achievements WHERE category='infection_wars'");
while($row_g = mysql_fetch_array($sql_g)){
    $aid = $row_g["id"];
    $aname = $row_g["name"];
    $adescr = $row_g["description"];
    $aarray = $row_g["achieved_by"];
    $aimg = $row_g["img"];
    
    $adescrNew = str_replace("ä", "&auml;", $adescr);
    $adescrNew = str_replace("ö", "&ouml;", $adescrNew);
    $adescrNew = str_replace("ü", "&uuml;", $adescrNew);
    $adescrNew = str_replace("Ä", "&Auml;", $adescrNew);
    $adescrNew = str_replace("Ö", "&Ouml;", $adescrNew);
    $adescrNew = str_replace("Ü", "&Uuml;", $adescrNew);
    $adescrNew = str_replace("ß", "&szlig;", $adescrNew);
    
    if($aimg == 0){
        $aimg = "0";
    } else {
        $aimg = $aid;
    }
    
    $aarray = explode(",", $aarray);
    
    if(in_array($id, $aarray)){
        echo '<li class="unlocked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    } else {
        echo '<li class="locked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    }
}

?>
<h1 class="sectionTitle">Deathmatch</h1>
<?php

$sql_g = mysql_query("SELECT * FROM achievements WHERE category='deathmatch'");
while($row_g = mysql_fetch_array($sql_g)){
    $aid = $row_g["id"];
    $aname = $row_g["name"];
    $adescr = $row_g["description"];
    $aarray = $row_g["achieved_by"];
    $aimg = $row_g["img"];
    
    $adescrNew = str_replace("ä", "&auml;", $adescr);
    $adescrNew = str_replace("ö", "&ouml;", $adescrNew);
    $adescrNew = str_replace("ü", "&uuml;", $adescrNew);
    $adescrNew = str_replace("Ä", "&Auml;", $adescrNew);
    $adescrNew = str_replace("Ö", "&Ouml;", $adescrNew);
    $adescrNew = str_replace("Ü", "&Uuml;", $adescrNew);
    $adescrNew = str_replace("ß", "&szlig;", $adescrNew);
    
    if($aimg == 0){
        $aimg = "0";
    } else {
        $aimg = $aid;
    }
    
    $aarray = explode(",", $aarray);
    
    if(in_array($id, $aarray)){
        echo '<li class="unlocked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    } else {
        echo '<li class="locked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    }
}

?>
<h1 class="sectionTitle">SoupPvP</h1>
<?php

$sql_g = mysql_query("SELECT * FROM achievements WHERE category='spvp'");
while($row_g = mysql_fetch_array($sql_g)){
    $aid = $row_g["id"];
    $aname = $row_g["name"];
    $adescr = $row_g["description"];
    $aarray = $row_g["achieved_by"];
    $aimg = $row_g["img"];
    
    $adescrNew = str_replace("ä", "&auml;", $adescr);
    $adescrNew = str_replace("ö", "&ouml;", $adescrNew);
    $adescrNew = str_replace("ü", "&uuml;", $adescrNew);
    $adescrNew = str_replace("Ä", "&Auml;", $adescrNew);
    $adescrNew = str_replace("Ö", "&Ouml;", $adescrNew);
    $adescrNew = str_replace("Ü", "&Uuml;", $adescrNew);
    $adescrNew = str_replace("ß", "&szlig;", $adescrNew);
    
    if($aimg == 0){
        $aimg = "0";
    } else {
        $aimg = $aid;
    }
    
    $aarray = explode(",", $aarray);
    
    if(in_array($id, $aarray)){
        echo '<li class="unlocked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    } else {
        echo '<li class="locked-achievement" style="padding: 6px; display: inline-block; width: 100%; height: 70px; background: none repeat scroll 0% 0% #DDD; margin: 0px 5px 9px; transition: all 0.3s ease 0s;"><p class="achievementDescription" style="margin-top: -12px"><div class="achievementIcon" style="float: left; margin: 3px; padding: 0px; display: block; background: #484F63; height: 65px; width: 65px;"><img src="/assets/images/achievements/' . $aimg . '.png" width="65" height="65" alt="" title=""/></div> <b>' . $aname . '</b><br/>' . $adescrNew . '</p></li>';
    }
}

?>