<div class="widget">
	<div class="subHeading">Support-Bereich - &Uuml;bersicht</div>
	<div style="margin: 10px; font-size: 13px;">
		<b><u>In-Game Support</u></b>
		<ul>
			<li><a href="/support">FAQ</a></li>
			<li><a href="/support/ticket/new">Ticket erstellen</a></li>
			<li><a href="/support/commands">Befehls-Liste</a></li>
			<li><a href="/support/benefits">Premium/VIP Vorteile</a></li>
			<li><a href="/support/terms-of-use/minecraft-server">Minecraft Regeln</a></li>
		</ul>
		<b><u>TeamSpeak Support</u></b>
		<ul>
			<li><a href="/support/terms-of-use/ts-server">TeamSpeak3 Regeln</a></li>
		</ul>
		<b><u>Store Support</u></b>
		<ul>
			<li><a href="/support/terms-of-use/store">Store Regeln</a></li>
		</ul>
	</div>
</div>
