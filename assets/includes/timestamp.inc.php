<?php
	function splitTimestamp($timestamp) {
		$dnt = explode(" ", $timestamp);
		$date = $dnt[0];
		$time = $dnt[1];
		
		$sd = explode("-", $date);
		$st = explode(":", $time);
		
		$year = $sd[0];
		$month = $sd[1];
		$day = $sd[2];
		
		$hours = $st[0];
		$minutes = $st[1];
		$seconds = $st[2];
		
		$split = array(
			"year" => $year,
			"month" => $month,
			"day" => $day,
			"hours" => $hours,
			"minutes" => $minutes,
			"seconds" => $seconds
		);
		return $split;
	}
?>