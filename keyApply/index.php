<?php require_once("../assets/includes/top.inc.php"); ?>
<?php

function get_client_ip() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

$error_message = "";
$success_message = "";

if(isset($_POST["submit"])){
    if(isset($_POST["name"]) && isset($_POST["why_you"]) && isset($_POST["skype"]) && isset($_POST["email"])){
        $name = $_POST["name"];
        $skype = $_POST["skype"];
        $email = $_POST["email"];
        $why_you = $_POST["why_you"];
        $ip = get_client_ip();
        $sql = mysql_query("SELECT * FROM key_applications WHERE username='" . $name . "' OR ip='" . $ip . "'");
        if(mysql_num_rows($sql) == 0){
            $sql3 = mysql_query("SELECT * FROM whitelist WHERE name='" . $name . "'");
            if(mysql_num_rows($sql3) == 0){
                $success_message = "Du hast dich erfolgreich beworben!<br/>Wir kontaktieren dich per Skype/E-Mail.";
                $sql2 = mysql_query("INSERT INTO `key_applications` (`username`, `skype`, `email`, `why`, `ip`) VALUES('" . $name . "', '" . $skype . "', '" . $email . "', '" . $why_you . "', '" . $ip . "');");
                echo mysql_error();
            } else {
                $error_message = "Du bist bereits gewhitelistet!";
            }
        } else {
            $error_message = "Du hast dich bereits beworben.";
        }
    } else {
        $error_message = "Bitte f&uuml;lle alle Felder aus!";
    }
}

?>
<div class="pageWidth">
    <div class="widget">
    	<div class="subHeading">Bewirb dich f&uuml;r einen Beta-Key!</div>
    	<div style="margin: 10px; font-size: 13px">
            <?php

                if($error_message != ""){
                    print '<div class="alert alert-failure"><p><i class="fa fa-ban"></i> ' . $error_message . '</p></div>';
                }

                if($success_message != ""){
                    print '<div class="alert alert-success"><p><i class="fa fa-check"></i> ' . $success_message . '</p></div>';
                }
            ?>
             <center>
                <form action="index.php" method="post">
                    <input class="huge-text" type="text" name="name" placeholder="Dein Minecraft Name.."/><br/>
                    <input class="huge-text" type="text" name="skype" placeholder="Dein Skype-Name.."/><br/>
                    <input class="huge-text" type="text" name="email" placeholder="Deine E-Mail Adresse.."/><br/>
                    <textarea name="why_you">Warum sollten wir dich auf unseren Server lassen? Beschreibe..</textarea>
                    <input type="submit" name="submit" value="Bewerbe"/><br/>
                </form>
            </center>
    	</div>
    </div>
</div>

<?php require_once("../assets/includes/bottom.inc.php"); ?>